import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import ProductPage from '../views/ProductPage.vue'
import Cont1 from '../views/Cont1.vue'
import Cont2 from '../views/Cont2.vue'
import Cont3 from '../views/Cont3.vue'
import Cont4 from '../views/Cont4.vue'
import Cont5 from '../views/Cont5.vue'
import Cont6 from '../views/Cont6.vue'
import Cont7 from '../views/Cont7.vue'
import Cont8 from '../views/Cont8.vue'
import Cont9 from '../views/Cont9.vue'
import Cont10 from '../views/Cont10.vue'
import Cont11 from '../views/Cont11.vue'
import Cont12 from '../views/Cont12.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/product',
    name: "ProductPage",
    component: ProductPage
  },
  {
    path: '/product/Cont1',
    name: "Cont1",
    component: Cont1
  },
  {
    path: '/product/Cont2',
    name: "Cont2",
    component: Cont2
  },{
    path: '/product/Cont3',
    name: "Cont3",
    component: Cont3
  },{
    path: '/product/Cont4',
    name: "Cont4",
    component: Cont4
  },{
    path: '/product/Cont5',
    name: "Cont5",
    component: Cont5
  },{
    path: '/product/Cont6',
    name: "Cont6",
    component: Cont6
  },{
    path: '/product/Cont7',
    name: "Cont7",
    component: Cont7
  },{
    path: '/product/Cont8',
    name: "Cont8",
    component: Cont8
  },{
    path: '/product/Cont9',
    name: "Cont9",
    component: Cont9
  },{
    path: '/product/Cont10',
    name: "Cont10",
    component: Cont10
  },{
    path: '/product/Cont11',
    name: "Cont11",
    component: Cont11
  },{
    path: '/product/Cont12',
    name: "Cont12",
    component: Cont12
  },
]

const router = new VueRouter({
  routes
})

export default router

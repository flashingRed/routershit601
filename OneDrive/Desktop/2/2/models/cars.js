const mongoose = require('mongoose')

const Cars = mongoose.Schema({
    model: {
        type: String,
        required: true,
        unique: true
    },
    companyName: {
        type: String,
        required: true,
    },
    xarakteristiki: {
        type: String,
        default: "nothing"
    },
    price: {
        type: Number,
        required: true
    },
    probeg: {
        type: Number,
        default: 0
    }
})

module.exports = mongoose.model("Cars", Cars)
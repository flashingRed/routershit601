module.exports = {
    products: require("./products.js"),
    users: require("./users.js"),
    cars: require("./cars.js"),
}
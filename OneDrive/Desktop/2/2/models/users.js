const mongoose = require('mongoose')

const Users = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    password: { 
        type: String,
        required: true
    },
    url: {
        type: String,
        default: "https://unsplash.com/photos/2DZY4rQzBSg"
    },
    productsCount: {
        type: Number,
        default: 0
    },
    products: {
        type: mongoose.Schema.Types.Array,
        ref: "Products",
    }
})

module.exports = mongoose.model("Users", Users) 
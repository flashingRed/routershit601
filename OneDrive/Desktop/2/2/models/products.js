const mongoose = require('mongoose')

const Products = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        default: "nothing"
    },
    price: {
        type: Number,
        required: true
    },
    usersId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true
    },
    numShow: {
        type: Number,
        default: 0
    }
})

module.exports = mongoose.model("Products", Products)
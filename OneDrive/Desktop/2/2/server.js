// Импортирует наш фреймворк
const express = require("express")

// Создаем главную переменннею
const app = express()

const bodyParser = require("body-parser");
const cors = require('cors')
const mongoose = require('mongoose')
const config = require("./config.js")

const PORT = process.env.PORT || 7777


app.use(cors())
app.use(bodyParser.json())

// Routes
app.use("/users", require("./routes/users.js"))
app.use("/products", require("./routes/products.js"))
app.use("/cars", require("./routes/cars.js"))

app.get('/', (req, res) => {
    res.send("<h1 style='font-family: Gilroy;'>Ti kiitoooo</h1>")
})

mongoose.connect(`mongodb+srv://akhmedovanis:${process.env.MONGO_PASSWORD}@cluster0.1bp9j.mongodb.net/?retryWrites=true&w=majority`, () => {
    console.log("Успешно подключились к global данных :)");
})

// if (process.env.NODE_ENV == "production") {
//     console.log("production");

//     // Global connect mondoDB
//     mongoose.connect(`mongodb+srv://akhmedovanis:${process.env.MONGO_PASSWORD}@cluster0.1bp9j.mongodb.net/?retryWrites=true&w=majority`, () => {
//         console.log("Успешно подключились к global данных :)");
//     })

//     mongoose.set("debug", true)
// } else {
//     console.log("development");
//     mongoose.connect("mongodb://localhost:27017/app", () => {
//         console.log("Успешно подключились к базе данных :)");
//     })

//     mongoose.connection.on('error', err => {
//         console.log(err);
//     })

//     mongoose.set("debug", true)
// }

// Слушатель по порту
app.listen(PORT, () => {
    console.log(`Сервер слушает порт: ${PORT}`);
})

// Можно перейти по домену localhost:8800
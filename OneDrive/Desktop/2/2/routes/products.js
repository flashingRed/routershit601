const express = require("express")
const router = express.Router()
const Products = require("../models/products")
const Users = require("../models/users")

router.get("/", (req, res) => {
    try {
        let products = Products.find().populate("usersId")

        res.json({
            ok: true,
            message: 'Продукты получены',
            data: products
        })
    }
    catch (error) {
        console.log(error);
    }
    console.log("Get all products! :)");
})

router.get("/:id", async (req, res) => {
    console.log('Get element: ' + req.params.id);

    try {
        let prod = await Products
            .findById(req.params.id)
            .populate("usersId")

        Products.findOneAndUpdate({ _id: req.params.id }, { $inc: { numShow: 1 } }, function (err, response) {
            if (err) {
                console.log(err);
            } else {
                console.log(response);
            }
        })

        res.json({
            ok: true,
            message: "Element found",
            data: prod
        })
    }
    catch (error) {
        console.log(error);
    }
})

router.patch("/:id", async (req, res) => {
    try {
        Products.findByIdAndUpdate(req.params.id, req.body, (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: "Element patch!",
                    element: data
                })
            }
        })
    }
    catch (error) {
        res.json({
            ok: false,
            message: "Seems there nogoods!",
            error
        })
    }
})

router.post("/", async (req, res) => {
    try {
        let products = await Products.find().populate("usersId")

        Products.create(req.body, async (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                let user = await Users.findById(data.usersId)

                user.products.push(data.id)
                user.productsCount = user.products.length
                user.save()

                res.json({
                    ok: true,
                    message: "Element created!",
                    element: products
                })
            }
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.delete("/:id", async (req, res) => {
    Products.findByIdAndDelete(req.params.id, async (error, data) => {
        if (error) {
            res.json({
                ok: false,
                messege: "Deleted shit!",
                el: data,
                error
            })
        } else {
            res.json({
                ok: true,
                message: 'Deleted',
                element: data,
            })
        }
    })
})

module.exports = router
const express = require("express")
const router = express.Router()
const Users = require("../models/users")

router.get("/", async (req, res) => {
    try {
        res.json({
            ok: true,
            message: 'Пользователи получены',
        })

    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.get("/:id", async (req, res) => {
    try {
        let users = await Users.findById(req.params.id)

        res.json({
            ok: true,
            message: 'Пользователь получен',
            data: users
        })

    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.patch("/:id", async (req, res) => {
    try {
        Users.findByIdAndUpdate(req.params.id, req.body, (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: "Element patch!",
                    element: data
                })
            }
        })
    }
    catch (error) {
        res.json({
            ok: false,
            message: "Seems there nogoods!",
            error
        })
    }
})

router.post("/", async (req, res) => {
    try {
        Users.create(req.body, async (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: "Element created!",
                    element: data
                })
            }
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.delete("/:id", async (req, res) => {
    Users.findByIdAndDelete(req.params.id, async (error, data) => {
        if (error) {
            res.json({
                ok: false,
                messege: "Deleted shit!",
                el: data,
                error
            })
        } else {
            res.json({
                ok: true,
                message: 'Deleted',
                element: data,
            })
        }
    })
})
module.exports = router
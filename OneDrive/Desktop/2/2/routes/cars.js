const express = require("express")
const router = express.Router()
const Cars = require("../models/cars")
 
router.get("/", async (req, res) => {
    
    try {
        let cars = await Cars.find()
        res.json({
            ok: true,
            message: 'Пользователи получены',
            cars

        })

    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.get("/:id", async (req, res) => {
    try {
        let car = await Cars.findById(req.params.id)

        res.json({
            ok: true,
            message: 'Пользователь получен',
            data: car
        })

    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.patch("/:id", async (req, res) => {
    try {
        Cars.findByIdAndUpdate(req.params.id, req.body, (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: "Element patch!",
                    element: data
                })
            }
        })
    } catch (error) {
        res.json({
            ok: false,
            message: "Seems there nogoods!",
            error
        })
    }
})

router.post("/", async (req, res) => {
    try {
        // let cars = await Cars.find().populate("usersId")

        Cars.create(req.body, async (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                // let user = await Users.findById(data.usersId)

                // user.cars.push(data.id)
                // user.carsCount = user.cars.length
                // user.save()

                res.json({
                    ok: true,
                    message: "Element created!",
                    
                })
            }
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.delete("/:id", async (req, res) => {
    Cars.findByIdAndDelete(req.params.id, async (error, data) => {
        if (error) {
            res.json({
                ok: false,
                messege: "Deleted shit!",
                el: data,
                error
            })
        } else {
            res.json({
                ok: true,
                message: 'Deleted',
                element: data,
            })
        }
    })
})

module.exports = router